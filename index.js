/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	let fullNamePrompt= prompt('What is your Name?');
	console.log('Hello, '+ fullNamePrompt);
	let agePrompt= prompt('How old are you?');
	console.log('You are '+agePrompt+' years old.');
	let addressPrompt= prompt('Where do you live?');
	console.log('You live in '+addressPrompt);


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

	
	
*/

	//second function here:

	function printFavoriteMusicArtists(){
		console.log('1. Valiant');
		console.log('2. Alkaline');
		console.log('3. Kanye West');
		console.log('4. J.Cole');
		console.log('5. Drake');
	};

	printFavoriteMusicArtists();
/*	

	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function printFavoriteMovies(){
		console.log('1. Rush Hour 1');
		console.log('Rotten Tomatoes Rating: 61%');

		console.log('2. Rush Hour 2 ');
		console.log('Rotten Tomatoes Rating: 51%');		

		console.log('3. Fight Club');
		console.log('Rotten Tomatoes Rating: 79%');

		console.log('4. Shutter Island');
		console.log('Rotten Tomatoes Rating: 68%');

		console.log('5. Time to Hunt');
		console.log('Rotten Tomatoes Rating: 65%');
	};

	printFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


		/*	printUsers();*/

		let printFriends =  function printUsers (){
			alert("Hi! Please add the names of your friends.");
			let friend1 = prompt("Enter your first friend's name:"); 
			let friend2 = prompt("Enter your second friend's name:"); 
			let friend3 = prompt("Enter your third friend's name:");

			console.log("You are friends with:");
			console.log(friend1); 
			console.log(friend2); 
			console.log(friend3); 
			}

			printFriends();

		




		
	
			